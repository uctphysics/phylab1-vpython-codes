from visual import *

objects = []



def runSimulation(ver=false,hor=false,airFric=0.0):
        global objects
        ballPosition = [0.0,-0.75,0.0]
        ballVelocity = [3.0,5.0,0.0]
        gravity = -10.0
        positionsToGraph = []
        velocitiesToGraph = []
        totalTime = ballVelocity[1]*2 / gravity
        displayScale = 120.0
        print "Gravity: ",gravity
        print "Initial position:",ballPosition
        print "Initial velocity:",ballVelocity
        print "Calculated time of simulation (s):",totalTime
        print "Calculating trajectory..."
        first = true
        ball = sphere(color=color.red,radius=5)
        objsToDestroy = []
        i = 0
        deltat = 0.001
        t = 0.0
        while (ballPosition[1]>-0.7499) or first:
                rate(200)
                first = false
                t+=deltat
                i+=1
                ballVelocity[1] += gravity*deltat
                for j in xrange(len(ballVelocity)):
                        if (ballVelocity[j]>0):
                                ballVelocity[j] -= deltat * airFric * ballVelocity[j]**2
                        else:
                                ballVelocity[j] += deltat * airFric * ballVelocity[j]**2
                                
                if (i%5==0):
                        tm = ballPosition[:]
                        for j in xrange(len(tm)):
                                tm[j]*=displayScale
                        tm[0] += ballVelocity[0] * totalTime /2.0 * displayScale
                        ball.pos = tm
                if (i%50) ==0:
                        tm = ballPosition[:]
                        for j in xrange(len(tm)):
                                tm[j]*=displayScale
                        tm[0] += ballVelocity[0] * totalTime /2.0 * displayScale
                        objects.append(sphere(pos = tm,radius=5,color=color.green))
                        tpp = tm[:]
                        if ver:
                                p = curve(radius=0.8,color=color.white)
                                p.append(tm)
                                tm[0] = 0
                                p.append(tm)
                                objects.append(sphere(pos = tm,radius=5,color=color.cyan))
                                objects.append(p)
                        tm = tpp[:]
                        if hor:
                                p = curve(radius=0.8,color=color.white)
                                p.append(tm)
                                tm[1] = -0.8 * displayScale
                                p.append(tm)
                                objects.append(p)
                                objects.append(sphere(pos = tm,radius=5,color=color.cyan))
                        
                for it in xrange(3):
                        ballPosition[it] += ballVelocity[it]*deltat
        print "Done"
        ball.visible = false
        objects.append(ball)

def killAll():
        for i in objects:
                i.visible = false
                del i
print "Welcome to the Newtonian Gravity simulator."
print "This program simulates a ball projected upwards and forwards under Newtonian gravity."
print "The results of this script can be checked by experiment"
print "NOTE: The author accepts no responsibility for any injury, harm or bodily damage"
print "      incurred by attempting to recreate the results.  Under no circumstances "
print "      should all the air be removed from a room full of people to test these results."
print "Have a nice day."
menu = \
"""Options (select one)
0) exit
1) no trails
2) horisontal trail
3) vertical trail
4) both trails
5) no trails, with air friction (approximated)
6) both trails, with air friction (approximated)

"""
while 1:
        print menu
        option = int(raw_input())
        killAll()
        if option==0:
                exit()
        if option==1:
                runSimulation()
        if option==2:
                runSimulation(hor=true)
        if option==3:
                runSimulation(ver=true)
        if option==4:
                runSimulation(hor=true,ver=true)
        if option==5:
                runSimulation(airFric=float(raw_input("Enter friction constant (between 0.1 and 0.5 is good): ")))
        if option==6:
                runSimulation(ver=true,hor=true,airFric=float(raw_input("Enter friction constant (between 0.1 and 0.5 is good): ")))
                 