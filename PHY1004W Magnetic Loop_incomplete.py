from visual import *

## Motion of a charged particle in the (dipole) magnetic field of a circular coil of current
## Based on the original code from Roger Fearick, September 2008 

## Define the circular current-carring loop
def makeloop(rloop,loopradius):
    dtheta=pi/100       # loop is made of segments of this size
    loop=curve(color=(1.,.7,.2),radius=0.01) # visualize loop
    for theta in arange(0.0,2.0*pi,dtheta):
        loop.append(pos=rloop+vector(loopradius*sin(theta),0.0,loopradius*cos(theta)))
    loop.append(loop.pos[0]) # close loop
    return loop
loop = makeloop(vector(0,0,0),0.3)

## Calculate the magnetic field at r due to the loop
## Note: (mu0/4pi)I arbitrarily set to 10.0
def field(r,loop):
    R=(loop.pos[0:-1]+loop.pos[1:])/2.0-r
    dl=loop.pos[1:]-loop.pos[0:-1]
    denom=mag(R)**3
    B1=cross(dl,R)/denom[:,newaxis]
    return sum(10.0*B1,axis=0)

## Display the magnetic field at a few positions (on the x-y plane)
showfields=1
if showfields:
    #print field(r0)
    for y in arange(-1.3,1.4,0.2):
        for x in arange(-1.3,1.4,0.2):
            rx=vector(x,y,0)
            B=field(rx,loop)
            B=B/100.0
            c=color.red
            if mag(B)>0.3:
                B*=0.3/mag(B)
                c=color.magenta
            arrow(pos=rx,axis=B,color=c)

## ============================================

## Model the motion of a charged particle in the magnetic field

## Take out all # below where appropriate and complete the program

## constants
# q = ...
# m = ...

## initial conditions
# t = ...
# r = ...
# v = ...

## integrate
# dt = 0.0005

# while t < 30:
    ## update clock time
    # t = ...
    ## B field at r
    # B = field(r,loop)
    ## update motion
    # F = ...
    # v = ...
    # r = ...




