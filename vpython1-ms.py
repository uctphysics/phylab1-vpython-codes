### This is an extention of the ball-in-a-box vpython tutorial, including a set of playing around.
### Feel free to play around, break and figure out how the various bits of this code fit together.
### If you're not too familiar with coding, this might be rather intimidating -- otherwise, enjoy!
###                                                                                 Maciek

from visual import *
from random import *

# Function to make vector of 3 random values in given range
def R3(low, lup):                               
    return (lup-low)*vector( random(), random(), random()) + low*vector(1,1,1);

# Function to tweak the ball color upon collision
def NewCol(oldcol):                             
    NewCol = vector(oldcol) + R3(-0.1, 0.1);
    for ind in range(3):
        NewCol[ind] = 1-abs(1-abs(NewCol[ind]));
    return NewCol;

BALL = [];                                      # Setup the balls (and their arrows) in a list
for i in range(input('How many balls? ')):
    r0 = R3(-5,5);
    v0 = R3(-20,20);
    c0 = R3(0,1);
    r = random()*.4+0.1;
    BALL.append([sphere(pos=r0, radius=r, color=c0, v=v0), arrow(pos=r0, axis = v0*r/4, color=c0)])
    
Wall = [ (6,0,0), (-6,0,0), (0,-6,0), (0,6,0), (0,0,-6) ];  # 5 walls
Size = [ (.2,12,12), (12,.2,12), (12,12,.2) ];              # but only 3 types
for i in range(5):                                  # Draw the walls
    box( pos=Wall[i], size = Size[i/2], color=(.5,.5,.5), opacity = 0.25 );

t = 0;
dt = 0.005;
scene.autoscale = 0;

while t < 1e3:                                  # Go go go!!!
    rate(100);
    for [ball,barr] in BALL:                    # One ball/arrow at a time
        ball.pos += ball.v * dt;                    # ball position
        barr.pos = ball.pos                         # arrow position
        
        for i in range(3):                      # Wall collision detection
            if abs(ball.pos[i]) >= 6-ball.radius-0.1:
                ball.v[i] *= -1                     # ball direction
                barr.axis = ball.radius*ball.v/4    # arrow direction
                ball.color = NewCol(ball.color)     
                barr.color = vector(ball.color)/2
    t += dt;
