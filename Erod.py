from visual import *
scene.x = scene.y = 0
scene.height = 800
scene.width = 600

kel = 9e9
Q = 1e-8
N = 50.
L = 1.0
dl = L/N
Escale = 3e-5
rod = []
for y in arange (-(L/2.)+(dl/2.), (L/2.), dl):
    a = sphere(pos=(0,y,0), color=color.red, radius=0.01, q=Q/N)
    rod.append(a)

obs = []
dy = L/4.
r = 0.05
for y in arange (-(L/2.), (L/2.)+dy, dy):
    for theta in arange(0,2*pi,(2*pi/6.)):
        pt = vector(r*cos(theta), y, r*sin(theta))
        obs.append(pt)

for pt in obs:
    E = vector(0,0,0)
    ar = arrow(pos=pt, color=(1,.5,0), axis=Escale*E, shaftwidth=0.01)
    for source in rod:
        r = pt - source.pos
        E = E + norm(r)*kel*source.q/mag(r)**2
        ar.axis = Escale*E
    if pt.y == 0:
        print '%e' %mag(E)
