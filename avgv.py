#! /usr/bin/python
from visual import *

print """
Illustration of an average velocity vector that varies in time,
tracing out a path.
Start with a (left) mouse click, after arranging zoom and rotation.
After the path is traced, another mouse click will permit you to trace out
the path as the mouse is moved.
"""

# position vector as fn. of t
def pos(t):
    return vector(t-0.01*t*t, 6.0 + 0.1*t -0.1*t**2 +0.005*t**3, 1.0-0.1*t+0.0033*t**3) 

# setup
t1=-5.0
t2start=-10.0

# axes
xax=arrow( shaftwidth=0.1, color=(0,1,0),pos=(0,0,0),axis=(10,0,0))
yax=arrow( shaftwidth=0.1, color=(0,1,0),pos=(0,0,0),axis=(0,10,0))
zax=arrow( shaftwidth=0.1, color=(0,1,0),pos=(0,0,0),axis=(0,0,10))
xl=label(pos=(10,-1,-1), text="X", box=0)
yl=label(pos=(-1,10,-1), text="Y", box=0)
zl=label(pos=(-1,-1,10), text="Z", box=0)

# path
path=curve( radius=0.05, color=(1.,0.7,0.2))
for t in arange( -10.0,10.0,0.1 ):
    path.append( pos=pos(t) ) 
# freeze scaling
scene.autoscale=0

# position vectors
r1=arrow(pos=(0,0,0),axis=pos(t1),color=(1,0,0),shaftwidth=0.2)
r2=arrow(pos=(0,0,0),axis=pos(t2start),color=(1,0,0),shaftwidth=0.2)
# displacement vector r2-r2
disp=arrow(pos=r1.axis,axis=r2.axis-r1.axis,color=(0,1,0.5),shaftwidth=0.1)
# caculated avg velocity (r2-r1)/(t2-t1)
avvel=arrow(pos=r1.axis,axis=(0.,0.,0.),color=(1,1,0),shaftwidth=0.3)
# show time interval
tx1=label(pos=(10,11,3), text="Delta t=%4.1f"%(t2start-t1), box=0)
tx2=label(pos=(10,9,3), text="Delta r=%4.1fi+%4.1fj+%4.1fk"%(avvel.axis.x,avvel.axis.y,avvel.axis.z) ,box=0)
tx3=label(pos=(10,-10,3), text="v=%4.1fi+%4.1fj+%4.1fk"%(avvel.axis.x,avvel.axis.y,avvel.axis.z))
# wait until ready ...
scene.mouse.getclick()
# r2 runs over path
for t2 in arange( t2start,10.0,0.1 ):
    rate(10)
    r2.axis=pos(t2)
    if t2-t1: avvel.axis=(r2.axis-r1.axis)/(t2-t1)
    disp.axis=r2.axis-r1.axis
    tx1.text="Delta t=%4.1f"%(t2-t1)
    tx2.text="Delta r=%4.1fi+%4.1fj+%4.1fk"%(disp.axis.x,disp.axis.y,disp.axis.z)
    tx3.text="v=%4.1fi+%4.1fj+%4.1fk"%(avvel.axis.x,avvel.axis.y,avvel.axis.z)
# now wait again ...
scene.mouse.getclick()
# r2 runs over path under mouse control
while 1:
    obs=scene.mouse.pos
    if not (obs==None):
        t2=obs.x
        #print m.pos
        r2.axis=pos(t2)
        if t2-t1: avvel.axis=(r2.axis-r1.axis)/(t2-t1)
        disp.axis=r2.axis-r1.axis
        tx1.text="Delta t=%4.1f"%(t2-t1)
        tx2.text="Delta r=%4.1fi+%4.1fj+%4.1fk"%(disp.axis.x,disp.axis.y,disp.axis.z)
        tx3.text="v=%4.1fi+%4.1fj+%4.1fk"%(avvel.axis.x,avvel.axis.y,avvel.axis.z)
