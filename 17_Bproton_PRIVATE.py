from visual import *
from __future__ import division
scene.x = scene.y = 0
scene.width = 800
scene.height = 600
##scene.forward = (-0.3,0,-1)
scene.background = color.white
scene.stereo="redcyan"


mzofp = 1e-7
L = 2e-10
##sphere(pos=(-1.3*L/2,0,0), radius=1e-20, color=color.black)

particle = sphere(pos=(-1e-10,0,0), radius=3e-12, color=color.red)
particle.v = vector(3e5,0,0)
pq = 1.6e-19
dt = 1e-18
Bscale = 12e-12  

# set up list of observation locations
olist = []
orad = 0.3e-10
for x in arange(-L/2, L/2, L/8):  
    for theta in arange(0,2*pi,pi/4):
        olist.append(arrow(pos=(x, orad*cos(theta), orad*sin(theta)),
                axis=(0,0,0),color=color.cyan))

# move proton, recalculate all B's at each position       
dt = 1e-18
scene.autoscale = 0
while 1:
    particle.x = -L/2
    while particle.x < L:
        rate(100)
        particle.pos = particle.pos + particle.v * dt
        for barrow in olist:
            r = barrow.pos - particle.pos
            B = mzofp*pq*cross(particle.v, norm(r))/mag(r)**2
            barrow.axis = B*Bscale
    ##        if abs(particle.pos.x) < 1.5e-13 and abs(barrow.pos.x) < 1e-12:
    ##            print mag(B)
    ## 
