#===========================================================
#
# microstates.py uct phy1004w IG 220408
#
#             calculates number of microstates for
#             distributing q quanta between N oscillators
# Formula:    A = (q+N-1)!/q!(N-1)!
#
# see also:   combin(x, y)
# 220408 IG python for phy1004w thermal physics
#
#===========================================================

from __future__ import division
from visual.factorial import *
from visual.graph import *
from Numeric import *

#####################################################################
# IG 2008
# code based on Gamma function from "Handbook of Mathematical Functions"
# by Abramowitz and Stegun
def LogGamma(N):    # natural log of gamma function used to find ln(N!)
                    # NB: Gamma(N+1) = N!
                    
    A = (N-0.5)*log(N) - N + 0.5*log(2*pi) + (12*N)**-1 + (360*N**3)**-1 - (1260*N**5)**-1 - (1680*N**7)**-1
    return A

N = 100.    # number of oscillators (change as needed)
q = 50.    # number of quanta (change as needed)

a = N + q - 1
b = q
p = a - b

X1 = LogGamma(a+1)
X2 = LogGamma(b+1)
X3 = LogGamma(p+1)

A = exp(X1 - X2 - X3)
###################################################################

# contributed by Chabay & Sherwood (authors: Matter & interactions)
B = combin(a,b)

###################################################################

# DGA 2005/6/7
def fact(x):        # uct phylab3 dga 060302
                    # fancy looking python code
    result=1
    while x > 1 :
        result *=x
        x -= 1
    return result

C = fact(a)/(fact(b)*fact(p)) # number of ways
############################################

print A, B, C # display results of IG, CS and DGA
