#! /usr/bin/python
from visual import *
"""
Orbit1-6.py 09/2006 rwf
Based on Orbit1-5.py
Simple numerical integration of 3-body system.
Circular orbit.
'sun' and 'earth' have same masses
a planet is added 
Initial conditions from An. Math. 152 (2000) 881-901
"""
G = 1.0   # grav. const.
t=0            # initial time (in s)
dt=0.002     # time step 1 day
# simulation objects
sun   = sphere( pos=(0.97000436,-0.24308753,0), radius=0.1, color=color.red )
star = sphere( pos=(-0.97000436,0.24308753,0), radius=0.1, color=color.yellow )
###star.pos=(4.5e10,0,0)  # TRY THIS
planet = sphere( pos=(0,0,0), radius=0.1, color=color.cyan )
sun.M=1.0
star.M=1.0
planet.M=0.999
star.p=star.M*vector(0.93240737,0.86473146,0.0)/2.0
sun.p=sun.M*vector(0.93240737,0.86473146,0.0)/2.0
# note that initial planet momentum is not in orbital plane of suns
planet.p=planet.M*vector(-0.93240737,-0.86473146,0.0)
# transform so initial momentum is zero and c.m. is at centre of screen
star.trail=curve(pos=star.pos, color=star.color)
planet.trail=curve(pos=planet.pos, color=planet.color)
sun.trail=curve(pos=sun.pos, color=sun.color)
# scene administration
scene.autoscale=0
# integration loop
while 1: #t < 10000*dt:
    rate(70)  # limit frame rate
    # now need to consider 3(3-1)/2=3 interactions
    R1=star.pos-sun.pos     # star by sun
    magR1=mag(R1)
    R2=star.pos-planet.pos  # star by planet
    magR2=mag(R2)
    R3=sun.pos-planet.pos    # sun by planet
    magR3=mag(R3)
    f1=-G*sun.M*star.M*R1/magR1**3
    f2=-G*planet.M*star.M*R2/magR2**3
    f3=-G*planet.M*sun.M*R3/magR3**3
    # N=3--body integration 
    star.p=star.p+(f1+f2)*dt
    star.pos=star.pos+(star.p/star.M)*dt
    star.trail.append(pos=star.pos)
    
    sun.p=sun.p+(f3-f1)*dt
    sun.pos=sun.pos+(sun.p/sun.M)*dt
    sun.trail.append(pos=sun.pos)

    planet.p=planet.p+(-f2-f3)*dt
    planet.pos=planet.pos+(planet.p/planet.M)*dt
    planet.trail.append(pos=planet.pos)
    t=t+dt
