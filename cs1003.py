#
# cs1003.py UCT PHY1004W dga 060510
#             2006 Thermal Chabay Sherwood Problem 10.3
#
#
# 060510 dga based on tpp06105.py
#
###################################################
from Numeric import *
from pylab import *

def fact(x):   # dga 060302 fancy python code
    result=1
    while x > 1 :
        result *=x  # dies for n > 170
        x -= 1
    return result

def lnfactorial(n):
   if n <= 170:          # for small n
      nfact = fact(n)
      return log(nfact) 

   else:
      return n*log(n) - n  # for large n use
                           # Stirling approximation to ln n!

    
###################################################################
n1 = 300.0               # initial data
n2 = 200.0
q  = 100.0
aq1    = 1.0*zeros(101)  # define arrays
alnom1 = 1.0*zeros(101)
alnom2 = 1.0*zeros(101)
alnom  = 1.0*zeros(101)
atemp1 = 1.0*zeros(101)
atemp2 = 1.0*zeros(101)
c = 3.0e+08
massa = 27.0*(938.0*1.0e+06)*(1.602e-19)/(c**2)
mass1 = (n1/3.0)*massa
mass2 = (n2/3.0)*massa
hbar = (6.6e-34)/(2*pi)
ym = 6.2e+10
atomd = 1.0e-10
kym = ym*atomd
ks = 4.0*kym     # spring constant 
kb = 1.4e-23    # Boltzmann's constant

m=0
for q1 in arange(0.0,q+.1,1.0):   #loop over q1
    q2 = q - q1
    lnom1 = lnfactorial(q1+n1-1) - lnfactorial(q1) - lnfactorial(n1-1)
    lnom2 = lnfactorial(q2+n2-1) - lnfactorial(q2) - lnfactorial(n2-1)
    lnom = lnom1 + lnom2
    #print q1, q2, lnom1, lnom2, lnom    # diagnostic
    # get temperature of the block
    lnom1a = lnfactorial(q1+1+n1-1) - lnfactorial(q1+1) - lnfactorial(n1-1)
    dels1 = kb*(lnom1a - lnom1)
    dele1 = 1.0*hbar*sqrt(ks/massa)
    temp1 = dele1/dels1
    lnom2a = lnfactorial(q2+1+n2-1) - lnfactorial(q2+1) - lnfactorial(n2-1)
    dels2 = kb*(lnom2a - lnom2)
    dele2 = 1.0*hbar*sqrt(ks/massa)
    temp2 = dele2/dels2
    print q1, q2, temp1, temp2
    aq1[m]       = q1           # fill arrays for graphing
    alnom1[m]    = lnom1
    alnom2[m]    = lnom2
    alnom[m]     = lnom
    atemp1[m]    = temp1
    atemp2[m]    = temp2
    m = m+1
    
title('CS Problem 10.3 (dga) 100 quanta over two blocks (300,200) osc')
xlabel('q1')
ylabel('temp_1(K), temp_2(K)')
#ylabel('ln(Omega_1), ln(Omega_2), ln(Omega)')
#plot(aq1,alnom1)          # plot graphs
#plot(aq1,alnom2)
#plot(aq1,alnom)
plot(aq1,atemp1)
show()
plot(aq1,atemp2)
show()                    # display all graphs
print 'end'
