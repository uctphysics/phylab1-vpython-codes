from visual import *
from visual.graph import *

## Scene setup
scene.width = 600.
scene.height = 480.
scene.x = scene.y = 0
scene.forward = (-0.2,-0.5,-1)
scene.background = color.black

## Main scale factors
xmax = 0.5
dx = xmax/5

## Draw floor
grid = []
for x in arange(-xmax, xmax+dx, dx):
    grid.append(curve(pos=[(x, 0, -xmax), (x, 0, xmax)], color=(.7, .7, .7)))
for z in arange(-xmax, xmax+dx, dx):
    grid.append(curve(pos=[(-xmax, 0, z), (xmax, 0, z)], color=(.7, .7, .7)))

## E and B fields
B0 = vector(0, 1e-6, 0)
E0 = vector(0, 0, 0)

## Display fields
bfield = []
bscale = 1e5
for x in arange(-xmax, xmax+dx, 2*dx):
    for z in arange(-xmax, xmax+dx, 2*dx):
        bfield.append(arrow(pos=(x, 0, z), axis=B0*bscale, color=(0, 1, 1)))
efield = []
escale = 1e5
for x in arange(-xmax, xmax+dx, 2*dx):
    for z in arange(-xmax, xmax+dx, 2*dx):
        efield.append(arrow(pos=(x, 0, z), axis=E0*escale, color=(1, 1, 0)))

## Particle properties
particle = sphere(pos=(-xmax/8.,2*dx,xmax/8.), color=(1,0,0), radius = dx/8.)
particle.charge = 1.6e-19
particle.mass = 1.7e-27
particle.velocity = vector(10, 0, 0)

## Force vector arrow
farrow = arrow(pos=particle.pos, axis=(0,0,0), color=(0.6,0.6,0.6))
fscale = 0.5e23

## Velocity vector arrow
varrow = arrow(pos=particle.pos, axis=(0,0,0), color=color.green)
vscale = 1e-2

## Time step
dt = 1e-6

## Particle trail
particle.trail = curve(color=particle.color)

## Plotting
## Fmagplot = gcurve()
## vmagplot = gcurve()

## Main calculation of Lorentz force

t = 0

while t < 1:
   if -xmax < particle.x < xmax and -xmax < particle.z < xmax:
       B = B0
       E = E0
   else:
       B = vector(0,0,0)
       E = vector(0,0,0)
   ## Calculate Lorentz force on particle    
   F = 
   ## Update velocity of particle
   particle.velocity = 
   ## Update position of particle
   particle.pos =

   particle.trail.append(pos=particle.pos)
   t = t + dt
   Fmag=mag(F)
   vmag = mag(particle.velocity)

## Scaling and moving arrows
   farrow.pos = particle.pos
   farrow.axis = F*fscale
   varrow.pos = particle.pos
   varrow.axis = particle.velocity*vscale
   
## Plotting   
##    Fmagplot.plot(pos = (t, Fmag))
##   vmagplot.plot(pos = (t, vmag))

