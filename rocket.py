from visual.graph import *


# some numbers for rocket ..
R=0.5 # size
M=1.0 # mass
c=0.5 # from I

F=1.0 # magnitude of force

# some new numbers ... replaces above
F=3.4
c=0.7
M=3.2
R=0.87

I=c*M*R**2  # moment of inertia of disc

# rotational motion. Take rotation relative to centre of mass.
theta=-pi/2
omega=0.0
# translational motion of c.m. p_trans=F_net
pcm=vector(0,0,0)
rcm=vector(0,0,0)
Fnet=vector(-F*sin(theta),F*cos(theta),0)


# graphics: plot pos. of c.m., and px, py as fn of time
cm=curve(pos=rcm,color=color.yellow,radius=0.05)
radiusvector=arrow(pos=rcm, axis=(R*cos(theta), R*sin(theta), 0),
                   shaftwidth=0.05)
thrustpoint=curve(pos=rcm+vector(R*cos(theta),R*sin(theta),0),
                  color=color.green,radius=0.03)
plot1=gcurve(color=color.yellow)
plot2=gcurve(color=color.cyan)

# initialize integration
t=0.0
dt=0.01

## print status for comparison with AJP.
## k1=sqrt((F/M)*pi*c*R) # prefactor of Fresnel C
## k2=sqrt(pi*c*R*M/F)   # time scale for Fresnel C
## print 'Max vx',k1*0.7798
## print 'At time', k2
## v0=0.0

# integrate --- use same 'update' structure as previous tuts.
while t<10.0:
    rate(100)
    # update clock time
    t=t+dt
    # calculate current force
    Fnet=vector(-F*sin(theta),F*cos(theta),0)
    # translational update -- note dependence on theta ... 
    pcm=pcm+Fnet*dt
    rcm=rcm+(pcm/M)*dt
    # rotational update ... now update theta
    omega=omega+F*R*dt/I
    theta=theta+omega*dt
    # calculate K.E. -- can plot this ...
    Ktrans=dot(pcm,pcm)/2/M
    Krot=0.5*I*omega**2
    # update graphics ...
    #cm.append(pos=(px,py,t))
    cm.append(pos=rcm)
    thrustpoint.append(pos=rcm+vector(R*cos(theta),R*sin(theta),0))
    radiusvector.pos=rcm
    radiusvector.axis=vector(R*cos(theta), R*sin(theta), 0)
    plot1.plot(pos=(t,pcm.x/M))
    plot2.plot(pos=(t,pcm.y/M))
##     if pcm.x/M <v0:
##         print t, pcm.x/M, t-dt, v0
##         break
##     else:
##         v0=pcm.x/M
    #plot1.plot(pos=(t,Ktrans))
    #plot2.plot(pos=(t,Krot))
