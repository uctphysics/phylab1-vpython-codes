from visual import *

d = display(x=0,y=0,height=600,width=800,title='magnetic field')

I = 5.0
kmag = 1e-7
L = 0.5
dx = L/20.
dl = vector(dx,0,0)
r1 = .05
r2 = .10
Bscale = 1.5e3
wire = []
for x in arange(-L/2.,L/2.,dx):
    c = cylinder(pos=(x,0,0), axis=dl, radius=0.01, color=(.7,.7,.7))
    wire.append(c)
    
arr = []
da = L/8.
for x in arange(-L/2., L/2.+da, da):
    for theta in arange(0, 2*pi, pi/4.0):
        b = arrow(pos=(x,r1*cos(theta), r1*sin(theta)), color=(0,1,1),
                       shaftwidth = 0.004, axis=(0,0,0))
        arr.append(b)
        b = arrow(pos=(x,r2*cos(theta), r2*sin(theta)), color=(0,1,1),
                       shaftwidth = 0.004, axis=(0,0,0))
        arr.append(b)

for s in wire:
    for a in arr:
        r = a.pos - s.pos
        a.axis = a.axis +Bscale*kmag*I*cross(dl, norm(r))/mag(r)**2

Iv = arrow(pos=(L/2.,0,0), axis=dl, color=(1,0,0))
