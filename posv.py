#!/usr/bin/python
from visual import *

print """
Illustration of a position vector that varies in time, tracing out a path.
Start with a (left) mouse click, after arranging zoom and rotation.
After the path is traced, another mouse click will permit you to trace out
the path as the mouse is moved.
"""

def pos(t):
    return vector(t-0.01*t*t, 6.0 + 0.1*t -0.1*t**2 +0.005*t**3, 1.0-0.1*t+0.0033*t**3) 

# coordinate axes
xax=arrow( shaftwidth=0.1, color=(0,1,0),pos=(0,0,0),axis=(10,0,0))
yax=arrow( shaftwidth=0.1, color=(0,1,0),pos=(0,0,0),axis=(0,10,0))
zax=arrow( shaftwidth=0.1, color=(0,1,0),pos=(0,0,0),axis=(0,0,10))
#our path
path=curve( radius=0.1, color=(1.,0.7,0.2))
# turn off autoscale
scene.autoscale=0
# initial values
tend=10.0
tstart=-10.0
dt=0.1
# the position vector
r2=arrow(pos=(0,0,0),axis=pos(tstart),color=(1,0,0),shaftwidth=0.2)
# show time and vector
tx1=label(pos=(10,10,3), text="t=%4.1f"%(tstart))
tx2=label(pos=(10,-10,3), text="%4.1fi+%4.1fj+%4.1fk"%(r2.axis.x,r2.axis.y,r2.axis.z))
# click to start
scene.mouse.getclick()
# trace out path
for t in arange( tstart,tend,dt ):
    rate(10)
    path.append( pos=pos(t) )    
    r2.axis=pos(t)
    tx1.text="t=%4.1f"%(t)
    tx2.text="%4.1fi+%4.1fj+%4.1fk"%(r2.axis.x,r2.axis.y,r2.axis.z)
# wait ....
scene.mouse.getclick()
# free visualisation of path
while 1:
    obs=scene.mouse.pos
    if not (obs==None):
        t=obs.x
        r2.axis=pos(t)
        tx1.text="t=%4.2f"%(t)
        tx2.text="%4.1fi+%4.1fj+%4.1fk"%(r2.axis.x,r2.axis.y,r2.axis.z)
    
