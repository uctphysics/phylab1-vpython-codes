#
# cs1004.py UCT PHY1004W dga 060511.0015
#                  2006 Thermal Chabay Sherwood Problem 10.3
#                  heat capacities per atom for Al and Pb
# 060510 dga based on cs1003.py
# 060510 dga based on tpp06105.py
#
###################################################
from Numeric import *
from pylab import *

def factorial(x):   # dga 060302 fancy python code
    result=1
    while x > 1 :
        result *=x  # dies for n > 170
        x -= 1
    return result

  
###################################################################
nosc  = 105              # model parameters
qmax  = 300
natom = nosc/3
                         # data (CS p 360)
ext   =  [ 20.0, 40.0, 60.0, 80.0,100.0,150.0,200.0,250.0,300.0,400.0]
excal =  [ 0.23, 2.09, 5.77, 9.65,13.04,18.52,21.58,23.25,24.32,25.61]
excpb =  [11.01,19.57,22.43,23.69,24.43,25.27,25.87,26.36,26.82,27.45]

tdata   = 1.0*zeros(len(ext))
caldata = 1.0*zeros(len(ext))
cpbdata = 1.0*zeros(len(ext))
navo = 6.023e+23              # Avogadro's number

for j in range(0,len(ext)):   # convert data to heat capacity per atom
    tdata[j] = ext[j]
    caldata[j] = excal[j]/(navo)
    cpbdata[j] = excpb[j]/(navo)



                           
atemp0   = 1.0*zeros(qmax+1)     #define arrays for graphs
ahcapaal = 1.0*zeros(qmax+1)
ahcapapb = 1.0*zeros(qmax+1)

c    = 3.0e+08          # speed of light
kb   = 1.4e-23          # Boltzmann's constant
hbar = (6.6e-34)/(2*pi) # Planck's constant

massh   = (938.0*1.0e+06)*(1.602e-19)/(c**2)  # atomic mass Hydrogen
aal     =  27.00                 # Atomic masses
apb     = 208.00
massal  = aal * massh
masspb  = apb * massh
ymal    = 6.2e+10
atomdal = 1.0e-10
kymal   = ymal*atomdal
ksal    = 5*kymal         # spring constant
ympb    = 0.50*ymal
atomdpb = atomdal
kympb   = ympb*atomdpb
kspb    = 5*kympb

m=0                            # Al Aluminium
for q in range(0, qmax+1,1):   #loop over q
    omega0 = (factorial(q+0+nosc-1)/factorial(q+0))/factorial(nosc-1)
    omega1 = (factorial(q+1+nosc-1)/factorial(q+1))/factorial(nosc-1)
    omega2 = (factorial(q+2+nosc-1)/factorial(q+2))/factorial(nosc-1)
    lnom0 = log(float(omega0))
    lnom1 = log(float(omega1))
    lnom2 = log(float(omega2))
    dels0 = kb*(lnom1 - lnom0)
    dels1 = kb*(lnom2 - lnom1)
    dele  = 1.0*hbar*sqrt(ksal/massal)  # Delta E
    temp0   = dele/dels0                # T = dE/dS
    temp1   = dele/dels1
    deltemp = temp1 - temp0             # Delta T
    hcapa   = (dele/natom)/deltemp      # Heat capacity per atom  
    atemp0[m]   = temp0          # fill arrays for graphing
    ahcapaal[m] = hcapa
    m = m+1
    
title('CS Problem 10.4 (dga) heat capacity  quanta over 1 blocks (105) osc')
xlabel('temp(K)')
ylabel('heat capacity C(J/K/atom)')

plot(atemp0,ahcapaal)           # plot graphs 
plot(tdata, caldata, 'bo')




m=0                             # Pb Lead
for q in range(0, qmax+1, 1):   # loop over q
    omega0 = (factorial(q+0+nosc-1)/factorial(q+0))/factorial(nosc-1)
    omega1 = (factorial(q+1+nosc-1)/factorial(q+1))/factorial(nosc-1)
    omega2 = (factorial(q+2+nosc-1)/factorial(q+2))/factorial(nosc-1)
    lnom0 = log(float(omega0))
    lnom1 = log(float(omega1))
    lnom2 = log(float(omega2))
    dels0 = kb*(lnom1 - lnom0)
    dels1 = kb*(lnom2 - lnom1)
    dele = 1.0*hbar*sqrt(kspb/masspb)
    temp0 = dele/dels0
    temp1 = dele/dels1
    deltemp = temp1 - temp0
    hcapa = (dele/natom)/deltemp
    atemp0[m]    = temp0           # fill arrays for graphing
    ahcapapb[m]    = hcapa
    m = m+1
    

plot(atemp0,ahcapapb)     # plot graphs 
plot(tdata,cpbdata,'go')

show()                    # display all graphs
print 'end'
