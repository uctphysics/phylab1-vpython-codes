from visual import *
ballPosition = [0.0,-0.75,0.0]
ballVelocity = [3.0,5.0,0.0]
gravity = -10.0
positionsToGraph = []
velocitiesToGraph = []
totalTime = ballVelocity[1]*2 / gravity
displayScale = 120.0
print "Gravity: ",gravity
print "Initial position:",ballPosition
print "Initial velocity:",ballVelocity
print "Calculated time of simulation (s):",totalTime

objsToDestroy = []
i = 0
deltat = 0.001
t = 0.0
first = true
print "Calculating trajectory..."
while (ballPosition[1]>-0.7499) or first:
	first = false
	t+=deltat
	i+=1
	ballVelocity[1] += gravity*deltat
	if (i%50) ==0:
		velocitiesToGraph.append(ballVelocity[:])
		positionsToGraph.append(ballPosition[:])
	for it in xrange(3):
		ballPosition[it] += ballVelocity[it]*deltat
print "Done"
for i in xrange(len(positionsToGraph)):
	rate(2)
	t = positionsToGraph[i][:]
	for j in xrange(len(t)):
		t[j]*=displayScale
	t[0] += ballVelocity[0] * totalTime /2.0 * displayScale
	s = velocitiesToGraph[i][:]
	for j in xrange(len(t)):
		s[j]*=3
	sphere(pos = t,color=color.green,radius=5)
	arrow(pos = t,axis=s,color=color.blue,shaftwidth=1)	

ex = raw_input("Show other stuff? (y/n)")
if ex=='n':
	exit()
for i in xrange(len(positionsToGraph)):
	t = positionsToGraph[i][:]
	for j in xrange(len(t)):
		t[j]*=displayScale
	t[0] += ballVelocity[0] * totalTime /2.0 * displayScale
	p = curve(color=color.white,radius=1)
	p.append(t)
	t[0] = 0
	p.append(t)
	objsToDestroy.append(p)
	objsToDestroy.append(sphere(pos = t,color=color.cyan,radius=5))
ex = raw_input("Show other stuff? (y/n)")
if ex=='n':
	exit()
for i in objsToDestroy:
	i.visible = false
	del i
for i in xrange(len(positionsToGraph)):
	t = positionsToGraph[i][:]
	for j in xrange(len(t)):
		t[j]*=displayScale
	t[0] += ballVelocity[0] * totalTime /2.0 * displayScale
	p = curve(color=color.white,radius=1)
	p.append(t)
	t[1] = -1 * displayScale
	p.append(t)
	sphere(pos = t,color=color.cyan,radius=5)