#! /usr/bin/python
from visual import *

def pos(t):
    return vector(t-0.01*t*t, 6.0 + 0.1*t -0.1*t**2 +0.005*t**3, 1.0-0.1*t+0.0033*t**3) 

# coordinate axes
xax=arrow( shaftwidth=0.1, color=(0,1,0),pos=(0,0,0),axis=(10,0,0))
yax=arrow( shaftwidth=0.1, color=(0,1,0),pos=(0,0,0),axis=(0,10,0))
zax=arrow( shaftwidth=0.1, color=(0,1,0),pos=(0,0,0),axis=(0,0,10))
#our path
path=curve( radius=0.1, color=(1.,0.7,0.2))
# turn off autoscale
scene.autoscale=0
# initial values
t0=-5.0
tend=10.0
tstart=-10.0
dt=0.1
# the position and displacement vectors
r1=arrow(pos=(0,0,0),axis=pos(t0),color=(1,0,0),shaftwidth=0.2)
r2=arrow(pos=(0,0,0),axis=(0,0,0),color=(1,0,0),shaftwidth=0.2)
disp=arrow(pos=r1.axis,axis=(r2.axis-r1.axis),color=(0,1,0.5),shaftwidth=0.1)
# show time and vector
tx1=label(pos=(10,10,3), text="t=%4.1f"%(tstart))
tx2=label(pos=(10,-10,3), text="%4.1fi+%4.1fj+%4.1fk"%(r2.axis.x,r2.axis.y,r2.axis.z))
# click to start
scene.mouse.getclick()
# trace out path
for t in arange( tstart,tend,dt ):
    rate(5)
    path.append( pos=pos(t) ) 
    r2.axis=pos(t)
    disp.axis=r2.axis-r1.axis
    tx1.text="t=%4.1f"%(t)
    tx2.text="%4.1fi+%4.1fj+%4.1fk"%(disp.axis.x,disp.axis.y,disp.axis.z)

# wait ....
scene.mouse.getclick()
# free visualisation of path
while 1:
    obs=scene.mouse.pos
    if not (obs==None):
        t=obs.x
        #print m.pos
        r2.axis=pos(t)
        disp.axis=r2.axis-r1.axis
        tx1.text="t=%4.1f"%(t)
        tx2.text="%4.1fi+%4.1fj+%4.1fk"%(disp.axis.x,disp.axis.y,disp.axis.z)
