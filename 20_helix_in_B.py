from visual import *

print """
Ruth Chabay Spring 2001
Magnetic force on a charge moving in a uniform magnetic field.
Click to start the motion, with proton initially outside field.
Then comment in the other initial position to start inside field.
"""

scene.width = scene.height = 1000.
scene.x = scene.y = 0
scene.forward = (-0.2,-0.5,-1)

xmax = .5
dx = xmax/5.
grid = []
for x in arange(-xmax, xmax+dx, dx):
    grid.append(curve(pos=[(x,0,-xmax),(x,0,xmax)], color=(.7,.7,.7)))
for z in arange(-xmax, xmax+dx, dx):
    grid.append(curve(pos=[(-xmax,0,z),(xmax,0,z)],color=(.7,.7,.7)))

## start proton outside region of uniform B
proton = sphere(pos=(xmax/2.,dx,2*xmax), color=(1,0,0), radius = dx/8.)

## start proton inside region of uniform B
##proton = sphere(pos=(-xmax/2.,dx,xmax/2.), color=(1,0,0), radius = dx/8.)

proton.mass = 1.7e-27
## initial momentum
proton.p = proton.mass*vector(0,1e6,-1e7)
##proton.p = proton.mass*vector(0,1e6,0)
proton.charge = 1.6e-19
proton.trail = curve(color=proton.color)


farrow = arrow(pos=proton.pos, axis=(0,0,0), color=(0.6,0.6,0.6))
fscale = 1.e11
varrow = arrow(pos=proton.pos, axis=(0,0,0), color=color.green)
vscale = 1e-8

B0 = vector(0,0.5,0)

bfield=[]
bscale = (xmax/3.)/mag(B0)
for x in arange(-xmax, xmax+dx, 2*dx):
    for z in arange(-xmax, xmax+dx, 2*dx):
        bfield.append(arrow(pos=(x,0,z), axis=B0*bscale, color=(0,.8,.8)))

dt = (xmax/(mag(proton.p)/proton.mass))/2000.
scene.mouse.getclick()
t = 0.

while proton.x < 2*xmax:
    if -xmax < proton.x < xmax and -xmax < proton.z < xmax:
        B = B0
    else:
        B = vector(0,0,0)
    v = proton.p / proton.mass
    F = proton.charge*cross(v,B)
    proton.p = proton.p + F*dt
    proton.pos = proton.pos + (proton.p/proton.mass)*dt
    farrow.pos = proton.pos
    farrow.axis = F*fscale
    varrow.pos = proton.pos
    varrow.axis = v*vscale
##    print F
    proton.trail.append(pos=proton.pos)
    rate(500)
