from __future__ import division
from visual import *

print '''
Questions to consider:
which object has the greater total momentum?
which object has the greater kinetic energy?
and 
which object has the greater translational kinetic energy?'''

scene.background = color.white
scene.x=0
scene.width=1000
scene.range = 20

visCM = 0       ## display arrows for vrel (1=True or 0=False)
visTOT = 0      ## instantaneous v of each ball
visREL = 0      ## display cm and vcm (True or False)
visTRAIL = 0    ## each ball leaves a trail
FARRVIS=0       ## force arrow visible

vscale=3*.5
fscale=5

w=1
L0=3.0*2
xmax = 14
yo = vector(0,1.5*w/2,0)
b1=sphere(pos=(-xmax,.8,0), radius=0.5, color=color.blue, m=1)
b1.trail = curve(color=b1.color, visible = visTRAIL)
spring1 = helix(pos=b1.pos, axis=(0,L0,0), radius=0.25,
               coils=10, thickness=0.1, color=(0.7,0.5,0),
                up = (0,0,1))
Lhat = norm(spring1.axis)
b2=sphere(pos=spring1.pos + spring1.axis, radius=0.5,color=color.blue, m=1)
b2.trail = curve(color=b2.color, visible = visTRAIL)
#
b1.p = vector(2,0,0)
b2.p = vector(2,0,0)
Fext = vector(0,0,0)
ks = 20000
#
b3=sphere(pos=(-xmax,-.8-L0,0), radius=0.5, color=color.red, m=1)
b3.trail = curve(color=b3.color, visible = visTRAIL)
spring2 = helix(pos=b3.pos, axis=(0,L0,0), radius=0.25,
               coils=10, thickness=0.1, color=(0.7,0.5,0),
                up = (0,0,1))
Lhat2 = norm(spring2.axis)
b4=sphere(pos=spring2.pos + spring1.axis, radius=0.5,color=color.red, m=1)
b4.trail = curve(color=b4.color, visible = visTRAIL)
#
b3.p = b1.p+vector(2,0,0)
b4.p = b2.p-vector(2,0,0)
#
cm1= sphere(pos=(b1.pos+b2.pos)/2, radius=0.1, color=color.cyan, visible=visCM)
cm2 = sphere(pos=(b3.pos+b4.pos)/2, radius=0.1, color=color.cyan, visible=visCM)
v3rel=arrow(pos=b3.pos, axis=(0,0,0), color=color.green,
            shaftwidth=0.2, fixedwidth=1, visible=visREL)
v4rel=arrow(pos=b4.pos, axis=(0,0,0), color=color.green,
            shaftwidth=0.2, fixedwidth=1, visible=visREL)
vcma1 = arrow(pos=cm1.pos,axis=(0,0,0), color=color.cyan,
             shaftwidth=0.2, fixedwidth=1,visible=visCM)
vcma2 = arrow(pos=cm2.pos,axis=(0,0,0), color=color.cyan,
             shaftwidth=0.2, fixedwidth=1,visible=visCM)
v3tot=arrow(pos=b3.pos, axis=(0,0,0), color=b3.color,
            shaftwidth=0.2, fixedwidth=1, visible=visTOT)
v4tot=arrow(pos=b4.pos, axis=(0,0,0), color=b4.color,
            shaftwidth=0.2, fixedwidth=1, visible=visTOT)

scene.autoscale = 0
scene.mouse.getclick()
Fext = vector(0,0,0)    ## no external force
F1 = vector(0,0,0)
dt = 0.0005
t = 0
while 1:
    rate(3000)
    if scene.mouse.clicked:         ## pause if mouseclick; wait for another
        scene.mouse.getclick()
        scene.mouse.getclick()
        
    if (b2.x > xmax*2) or (b1.x > xmax*2):   ## wrap
        b1.pos = b1.pos + (-3.5*xmax,0,0)
        b2.pos = b2.pos + (-3.5*xmax,0,0)
        b3.pos = b3.pos + (-3.5*xmax,0,0)
        b4.pos = b4.pos + (-3.5*xmax,0,0)

    Lhat = norm(b2.pos-b1.pos)
    s1 = (mag(b2.pos - b1.pos))-L0
    Fspring = -ks*s1*Lhat            ## force on ball 2 (blue) by spring
    b2.p= b2.p + (Fext+Fspring)*dt
    b1.p = b1.p +(F1- Fspring)*dt
    dr2 = (b2.p/b2.m)*dt
    b2.pos = b2.pos + dr2
    b2.trail.append(pos=b2.pos)
    dr1 =(b1.p/b1.m)*dt
    b1.pos = b1.pos + dr1
    b1.trail.append(pos=b1.pos)
    spring1.pos = b1.pos
    spring1.axis = b2.pos - b1.pos

    Lhat = norm(b4.pos-b3.pos)
    s1 = (mag(b4.pos - b3.pos))-L0
    Fspring = -ks*s1*Lhat            ## force on ball 2 (blue) by spring
    b4.p= b4.p + (Fext+Fspring)*dt
    b3.p = b3.p +(F1- Fspring)*dt
    dr2 = (b4.p/b4.m)*dt
    b4.pos = b4.pos + dr2
    b4.trail.append(pos=b4.pos)
    dr1 =(b3.p/b3.m)*dt
    b3.pos = b3.pos + dr1
    b3.trail.append(pos=b3.pos)
    spring2.pos = b3.pos
    spring2.axis = b4.pos - b3.pos

    
    cm2.pos=(b3.pos+b4.pos)/2
    cm1.pos=(b1.pos+b2.pos)/2

    vcm2 = (b3.p+b4.p)/(b3.m+b4.m)
    vcm1 = (b1.p+b2.p)/(b1.m+b2.m)
    
    v3rel.axis=((b3.p/b3.m)-vcm2)*vscale
    v3rel.pos=b3.pos
    v4rel.axis= ((b4.p/b4.m)-vcm2)*vscale
    v4rel.pos=b4.pos
    v4tot.pos = b4.pos
    v4tot.axis = (b4.p/b4.m)*vscale
    v3tot.pos = b3.pos
    v3tot.axis = (b3.p/b3.m)*vscale
    
    vcma2.pos=cm2.pos
    vcma2.axis=vcm2*vscale
    vcma1.pos=cm1.pos
    vcma1.axis=vcm1*vscale
