#! /usr/bin/python
from visual import *

print """
Projectile motion in 3-d .
Use the mouse to zoom and rotate.
Then (left) click to run with no air resistance
Another click runs (cricket ball) with air resistance.
Another click runs (tennis ball) with air resistance .
"""
def Reyn( v, radius):
    return 2.*radius*airden*abs(v)/airvisc

def Cd( Rn ):
    if Rn < 0.001: return 0.0
    else:
        return (24.0/Rn) + (6.0/(1.0+sqrt(Rn)))+0.4
        
def shoot( theta, v0, airden, colour ):
    theta = theta*pi/180
    v=vector(v0*cos(theta),v0*sin(theta),0.0)
    r=vector(-12.0,y0,0.0)
    path=curve( radius=0.1, color=colour)
    a0=vector(0,-g,0)
    while 1:
        v=v+(a0-0.5*airden*Cd(Reyn(mag(v),radius))*area1*v*mag(v)/m1)*dt
        r=r+v*dt
        #print r
        path.append(pos=r)
        if r[1] < y0: break



airden=1.17
airvisc=1.85e-5
radius=0.03
den1=1200.
dt=0.001
m1=(4.0/3.0)*pi*radius**3*den1
print 'm1=',m1
g=9.81
area1=pi*radius**2

y0=-8.0
ground=array([[-12.0,y0,0.0],[12.0,y0,0.0]])
path=curve( radius=0.1, pos=ground,color=(0.,1.0,0.2))

scene.autoscale=0
scene.mouse.getclick()
# trace out path at constant velocity

gold=(1.0,0.7,0.2)
pink=(1.0,0.7,0.7)

v0=15.0
shoot( 45.0, v0, 0.0, pink)
scene.mouse.getclick()
shoot( 30.0, v0, 0.0, pink)
shoot( 60.0, v0, 0.0, pink)

scene.mouse.getclick()
shoot( 45.0, v0, airden, gold)
shoot( 30.0, v0, airden, gold)
shoot( 60.0, v0, airden, gold)


scene.mouse.getclick()
den1=200.
m1=(4.0/3.0)*pi*radius**3*den1
print 'm1=',m1
shoot( 45.0, v0, airden, gold)
shoot( 30.0, v0, airden, gold)
shoot( 60.0, v0, airden, gold)
