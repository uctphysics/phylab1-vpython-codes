from visual import * 

"""
ball bouncing in a box
"""

# create ball
ball = sphere(pos=(-5,0,0), radius=0.5, color=color.cyan) 
#create sides of box
wallR = box(pos=(6,0,0), size=(0.2,12,12), color=color.green) 
wallL = box(pos=(-6,0,0), size=(0.2,12,12), color=color.green) 
wallT = box(pos=(0,6,0), size=(12,0.2,12), color=color.green) 
wallB = box(pos=(0,-6,0), size=(12,0.2,12), color=color.green)
# initial ball attributes
ball.velocity = vector(25,5,2)
ball.trail=curve(color=ball.color)
ball.vscale = 0.1 
ball.varr = arrow(pos=ball.pos, axis=ball.vscale*ball.velocity, color=color.yellow)
# initialize time
deltat = 0.005 
t=0
# keep graphics window from rescaling
scene.autoscale=0
# loop for some time...
while t < 10.0:
    # limit computer screen update rate
    rate(100)
    # update ball veocity
    if ball.pos.x > wallR.pos.x or ball.pos.x < wallL.pos.x: 
        ball.velocity.x = -ball.velocity.x 
    if ball.pos.y > wallT.pos.y or ball.pos.y < wallB.pos.y: 
        ball.velocity.y = -ball.velocity.y
    # positions of walls in 'z' direction are ''virtual': no actual object
    if ball.pos.z > 6 or ball.pos.z < -6: 
        ball.velocity.z = -ball.velocity.z 
    # update ball position
    ball.pos = ball.pos + ball.velocity*deltat
    # update ball attributes
    ball.varr.pos=ball.pos
    ball.varr.axis=ball.vscale*ball.velocity
    ball.trail.append(pos=ball.pos)
    # update time
    t = t + deltat
