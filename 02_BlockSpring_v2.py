from __future__ import division
from visual import *
from visual.graph import *
scene.background = color.white
scene.height = 800
scene.width = 250
scene.x = scene.y =0
## Ruth Chabay 2007-01-25  revised Fall 2009
print """
Click anywhere in display window (not graph) to step.
Red arrow: Force;   Blue arrow: momentum.
Change delta_t to 0.01 (uncomment) to get 100 steps (no clicking). """

## CONSTANTS
delta_t = 0.1   ## for 3-10 steps
##delta_t = 0.01   ## for 100 steps
mblock = 0.06
ks = 8
L0 = 0.2
## OBJECTS & INITIAL VALUES
block=box(pos=(0,0.1,0), size=(0.03,0.03,0.03), color=color.magenta,
          opacity=0.5)
spring = helix(pos=(0,0,0), axis=(0,0.1,0), radius=0.01,
               thickness = 0.002, coils=20, color=color.yellow)
pblock = mblock*vector(0,0,0)
offset = vector(-0.03,0,0)
sw = 0.01
Farr = arrow(color=color.red, shaftwidth=sw, axis=(0,0,0), fixedwidth=1,
             pos=block.pos+offset)
parr = arrow(color=color.blue, shaftwidth=sw, axis=(0,0,0), fixedwidth=1,
             pos=block.pos)

# start time at 0
t = 0
scene.center = (0,.1,0)    # move camera up
scene.range = 0.15

## GRAPH STUFF
gd = gdisplay(background=color.white, y=0, x=250, height=800,
              ymax = 0.42/2, ymin=-0.06)
blocky = gcurve(color=block.color)
blocky.plot(pos=(t,block.pos.y))  ## initial pos.y of block

Fgrav = vector(0,-9.8*mblock,0)
scene.mouse.getclick()

fscale = 5e-1
pscale = 4.0

## CALCULATIONS
while t < 1.01:
    rate(5) ## only 5 iterations per second
    L = block.pos - vector(0,0,0)  # a vector
    Lhat = norm(L)
    Fspring= -ks*(mag(L)-L0)*Lhat
    Fnet = Fspring + Fgrav
    Farr.axis = Fnet*fscale
    if delta_t > 0.01:
        scene.mouse.getclick()  # wait for a click if only 3-10 steps
    pblock = pblock + Fnet*delta_t
    parr.pos = block.pos
    parr.axis = pblock*pscale
    if delta_t > 0.01:
        scene.mouse.getclick()  # wait for a click if only 3-10 steps    
    block.pos = block.pos+(pblock/mblock)*delta_t
    Farr.pos = block.pos + offset
    parr.pos = block.pos
    spring.axis = block.pos-vector(0,0,0)

    # update time
    t = t + delta_t
    # plot pos.y of block
    blocky.plot(pos=(t,block.pos.y))
##    print t, block.y

print 'Done'


