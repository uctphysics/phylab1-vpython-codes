#! /usr/bin/python
import os
import sys
from ftplib import FTP

args = sys.argv[:]
#indexfilename = 'compphy.htm'
#args[0]=indexfilename
connection=FTP('www.phy.uct.ac.za','physics','Kevlin32')
print 'Connected' 
#connection.login()
connection.cwd('phy1004w')
for filename in args[1:]:
    print 'Put ',filename
    localfile=open( filename, 'r' )
    connection.storbinary('STOR '+filename, localfile, 1024)
    localfile.close()
connection.quit()
print ' .. done'
