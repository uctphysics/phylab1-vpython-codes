# evaluate the electric field along the midpoint of a uniformly charged thin rod

k = 9.0e9
Q = 1.0e-9
L = 1.0

x = 0.05

n_bits = 10

print '\n'
print 'k=%.0f'%k
print 'Q=%.10f'%Q 
print 'L=%.1f'%L  
print 'x=%.3f'%x  
print 'n_bits=%.0f'%n_bits
print '\n'

delta_y = L/n_bits
y_max = L/2
y_min = -L/2

y_positions = []
for i in range(0,n_bits):
    y_positions.append( y_max - delta_y/2 - i*delta_y )

E_x = 0
E_y = 0

print '  y       delta_E_x    delta_E_y'

for y in y_positions:

    delta_E_x = k * (Q/L) * ( x  / (x**2+y**2)**1.5 ) * delta_y
    delta_E_y = k * (Q/L) * ( -y / (x**2+y**2)**1.5 ) * delta_y
    
    print '{0:5.2f}, {1:10.5f}, {2:10.5f}'.format(y,delta_E_x,delta_E_y) 

    E_x += delta_E_x
    E_y += delta_E_y

print '------------------------------------'
print 'Sum    {0:10.5f}, {1:10.5f}'.format(E_x,E_y)
