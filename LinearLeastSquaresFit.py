import numpy as np


# enter the data and get the number of data points

xdata = np.array([0.0, 1.0, 2.0, 3.0])                
ydata = np.array([2.1, 3.3, 3.8, 4.5])
N = len(xdata)


# calculate the sums needed for the least squares fit

sum_xy = 0.0
sum_x = 0.0
sum_y = 0.0
sum_xx = 0.0
sum_dd = 0.0;

for j in range(N):
       
        sum_xy += xdata[j]*ydata[j]
        sum_x += xdata[j]
        sum_y += ydata[j]
        sum_xx += xdata[j]*xdata[j]


# do the linear least squares fit
# (compare these to equations on page 110 of the measurement manual)

mfit = (N*sum_xy - sum_x * sum_y) / (N*sum_xx - sum_x * sum_x)
cfit = (sum_xx*sum_y - sum_xy*sum_x) / (N*sum_xx - sum_x * sum_x)

for j in range(N):
        d = ydata[j] - (mfit * xdata[j] + cfit)
        sum_dd += d*d;

umfit = np.sqrt((sum_dd)/(N*sum_xx - sum_x * sum_x)*N/(N-2))
ucfit = np.sqrt((sum_dd * sum_xx)/(N*(N*sum_xx - sum_x * sum_x))*N/(N-2))

# print the results

print("m = %12f +/- %10f"%(mfit,umfit))
print("c = %12f +/- %10f"%(cfit,ucfit))


# plot the model prediction with the best-fit parameters 
# (you will need matplotlib installed to do this)

#import matplotlib.pyplot as plt
    
#plt.errorbar(xdata, ydata, label = "Data")

#yfit = mfit*xdata + cfit
#plt.plot(xdata, yfit, '-r', label = "Unweighted Best Fit")

## include legend and show figures ##

#plt.title("The result of the model fit to the linear data")
#plt.ylabel("y")
#plt.xlabel("x")
#plt.legend()
#plt.show()
