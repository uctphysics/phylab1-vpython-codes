from visual import *

"""
Calculate the electric field near a charged rod 
"""

# rod
L=1.0 # length [m]
Q=1e-9 # charge [C]
x=0.05 # observation point (from centre of rod) [m]

# integration
Nslice=20
Delta_y=L/Nslice

# visualization
scale=0.002
Rod=curve(pos=[(0,-L/2,0),(0,L/2,0)],radius=0.02)
Delta=curve(pos=[(0,-L/2,-L/2+Delta_y),(0,L/2,0)],radius=0.02,color=color.magenta)
Esum=arrow(pos=vector(x,0,0),axis=(0,0,0),color=color.red,shaftwidth=0.01)

# integrate
for i in range(Nslice):
    rate(2) # take it real slow ...
    # calculate contribution to integral from dy
    y=-L/2+i*Delta_y
    r=vector(x,y+Delta_y/2,0)
    magr=sqrt(x**2+(y+Delta_y/2)**2)
    rhat=r/magr
    E=9e9*(Q/L)*Delta_y/magr**2
    # visualize
    Delta.pos=[(0,y,0),(0,y+Delta_y,0)]
    Evec=E*rhat
    Delta_E=arrow(pos=(x,0,0),axis=scale*Evec,shaftwidth=0.01)
    # sum to get integral (scaled)
    Esum.axis=Esum.axis+Delta_E.axis
# result
print "E_x=",Esum.axis.x/scale, "N/C"
