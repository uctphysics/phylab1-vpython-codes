#! /usr/bin/python
from visual import *

print """
Motion in 3-d with constant velocity and constant acceleration.
Illustrates the decomposition of the motion into 3 vectors,
r=r0 + v0 t + 0.5 a t**2
Use the mouse to zoom and rotate.
Then (left) click to run at constant v
Another click adds the acceleration vector.
Another click runs at constant a.
Various labels are added at the end, for use in discussion in the lecture.
"""

# draw some coordinate axes
xax=arrow( shaftwidth=0.1, color=(0,1,0),pos=(0,0,0),axis=(10,0,0))
yax=arrow( shaftwidth=0.1, color=(0,1,0),pos=(0,0,0),axis=(0,10,0))
zax=arrow( shaftwidth=0.1, color=(0,1,0),pos=(0,0,0),axis=(0,0,10))
# set up our two paths
path=curve( radius=0.1, color=(1.,0.7,0.2))
path2=curve( radius=0.1, color=(1.,0.7,0.2))

scene.autoscale=0
scene.background=(0.6,0.6,0.6)
t0=0.
#define initial position and velocity
r1=arrow(pos=(0,0,0),axis=(-12,5,-1),color=(1,0,0),shaftwidth=0.2)
v=arrow(pos=(-4,6,-1),axis=(3,-1,2),color=(0,0,1),shaftwidth=0.2)
label(pos=v.pos,text="v",box=0,line=0,xoffset=-10)
#r2=v0 t, r3=r1+r2
r2=arrow(pos=r1.axis,axis=v.axis*0.1*t0,color=(1,0,0),shaftwidth=0.2)
r3=arrow(pos=(0,0,0),axis=v.axis*t0*0.1+r1.axis,color=(1,1,0),shaftwidth=0.2)
#wait for click
scene.mouse.getclick()
# trace out path at constant velocity
for t in arange( t0,50.,0.5 ):
    rate(10)
    path.append(pos=v.axis*t*0.1+r1.axis)
    r2.axis=v.axis*t*0.1
    r3.axis=v.axis*t*0.1+r1.axis
    #r4.pos=v.axis*t*0.1+r1.axis
    path2.append(pos=v.axis*t*0.1+r1.axis)
# wait ...
scene.mouse.getclick()
# add constant acceleration
a=arrow(pos=(-4,9,-1),axis=(-0.1,0.9,0.2),color=(0,0.5,1),shaftwidth=0.2)
label(pos=a.pos,text="a",box=0,line=0,xoffset=-10)
# reset these to leave prvoius path on display
r2.axis=(0,0,0)
r3.axis=(0,0,0)
# add r4=0.5 a0 t**2
r4=arrow(pos=v.axis*t0*0.1+r1.axis,axis=a.axis*t0*t0*0.005,color=(1,0,0),shaftwidth=0.2)
# and wait
scene.mouse.getclick()
# now animate vectors at constant a
for t in arange( t0,50.,0.5 ):
    rate(10)
    path.append(pos=v.axis*t*0.1+r1.axis)
    r2.axis=v.axis*t*0.1
    r3.axis=v.axis*t*0.1+r1.axis+a.axis*t*t*0.005
    r4.pos=v.axis*t*0.1+r1.axis
    r4.axis=a.axis*t*t*0.005
    path2.append(pos=v.axis*t*0.1+r1.axis+a.axis*t*t*0.005)

#finally, add some labels for explanation
label(pos=r1.axis/2,text="r0",box=0)
r2l=label(pos=r1.axis+r2.axis/2,text="dr1",space=1.,box=0)
r3l=label(pos=r3.axis/2,text="R",space=1.)
r4l=label(pos=r1.axis+r2.axis+r4.axis/2,text="dr2",space=1.)
