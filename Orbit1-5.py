#! /usr/bin/python
from visual import *
"""
Orbit1-5.py 04/2006 rwf
Based on Orbit1-4.py
Simple numerical integration of 3-body system.
Circular orbit.
'sun' and 'earth' have same masses differing by 2
a planet is added 
Initial transform to frame moving with centre of mass
"""
G = 6.67e-11   # grav. const.
t=0            # initial time (in s)
dt=24*3600     # time step 1 day
#scene.stereo="redblue"
# simulation objects
sun   = sphere( pos=(0,0,0), radius=1.5e10, color=color.red )
star = sphere( pos=(1.5e11,0,0), radius=1e10, color=color.yellow )
###star.pos=(4.5e10,0,0)  # TRY THIS
planet = sphere( pos=(2.5e11,0,0), radius=6.4e9, color=color.cyan )
sun.M=2e30
star.M=1e30
planet.M=6e27
star.p=star.M*vector(0,sqrt(G*sun.M/mag(star.pos-sun.pos)),0)
sun.p=sun.M*vector(0,0,0)
# note that initial planet momentum is not in orbital plane of suns
planet.p=planet.M*vector(0,-sqrt(G*(sun.M+star.M)/mag(planet.pos-sun.pos)),2000)
# transform so initial momentum is zero and c.m. is at centre of screen
ptot=sun.p+star.p
vtot=ptot/(sun.M+star.M)
rtot=(star.M*star.pos+sun.M*sun.pos+planet.M*planet.pos)/(star.M+sun.M+planet.M)
sun.pos-=rtot
star.pos-=rtot
planet.pos-=rtot
star.trail=curve(pos=star.pos, color=star.color)
planet.trail=curve(pos=planet.pos, color=planet.color)
sun.trail=curve(pos=sun.pos, color=sun.color)
sun.p=sun.p-vtot*sun.M
star.p=star.p-vtot*star.M
# scene administration
scene.autoscale=0
# integration loop
while t < 5000*dt:
    rate(50)  # limit frame rate
    # now need to consider 3(3-1)/2=3 interactions
    R1=star.pos-sun.pos     # star by sun
    magR1=mag(R1)
    R2=star.pos-planet.pos  # star by planet
    magR2=mag(R2)
    R3=sun.pos-planet.pos    # sun by planet
    magR3=mag(R3)
    f1=-G*sun.M*star.M*R1/magR1**3
    f2=-G*planet.M*star.M*R2/magR2**3
    f3=-G*planet.M*sun.M*R3/magR3**3
    # N=3--body integration 
    star.p=star.p+(f1+f2)*dt
    star.pos=star.pos+(star.p/star.M)*dt
    star.trail.append(pos=star.pos)
    
    sun.p=sun.p+(f3-f1)*dt
    sun.pos=sun.pos+(sun.p/sun.M)*dt
    sun.trail.append(pos=sun.pos)

    planet.p=planet.p+(-f2-f3)*dt
    planet.pos=planet.pos+(planet.p/planet.M)*dt
    planet.trail.append(pos=planet.pos)
    t=t+dt
