#! /usr/bin/python
from visual import *

print """
Motion in 3-d with constant velocity and constant acceleration.
Illustrates the decomposition of the motion into 3 vectors,
r=r0 + v0 t + 0.5 a t**2
Use the mouse to zoom and rotate.
Then (left) click to run at constant v
Another click adds the acceleration vector.
Another click runs at constant a.
Various labels are added at the end, for use in discussion in the lecture.
"""

# draw some coordinate axes
xax=arrow( shaftwidth=0.1, color=(0,1,0),pos=(0,0,0),axis=(10,0,0))
yax=arrow( shaftwidth=0.1, color=(0,1,0),pos=(0,0,0),axis=(0,10,0))
zax=arrow( shaftwidth=0.1, color=(0,1,0),pos=(0,0,0),axis=(0,0,10))
# set up our two paths
path=curve( radius=0.1, color=(1.,0.7,0.2))
path2=curve( radius=0.1, color=(1.,0.7,0.2))

scene.autoscale=0
t0=0.
#define initial position and velocity
r1=arrow(pos=(-10,0,0),axis=(0,0,0),color=(1,0,0),shaftwidth=0.15)
v=arrow(pos=(-10,0,0),axis=(4,3,0),color=(0,0,1),shaftwidth=0.15)
#label(pos=v.pos,text="v",box=0,line=0,xoffset=-10)
#r2=v0 t, r3=r1+r2
#r2=arrow(pos=r1.axis,axis=v.axis*0.1*t0,color=(1,0,0),shaftwidth=0.2)
#r3=arrow(pos=(0,0,0),axis=v.axis*t0*0.1+r1.axis,color=(1,1,0),shaftwidth=0.2)
#wait for click
theta=atan(v.axis.y/v.axis.x)
t1=(10.0-v.pos.x)/v.axis.x
monkey=sphere(pos=(r1.pos+vector((t1-t0)*v.axis.x,(t1-t0)*v.axis.y,0)),radius=0.5)
mstart=monkey.pos
r2=arrow(pos=r1.pos,axis=(0,0,0),color=(1,0,0),shaftwidth=0.15)
r3=arrow(pos=monkey.pos,axis=(0,0,0),color=(1,1,0),shaftwidth=0.15)
scene.mouse.getclick()
# trace out path at constant velocity
for t in arange( t0,t1,0.01 ):
    rate(100)
    path.append(pos=v.axis*t+r1.pos)
    r1.axis=v.axis*t
    #r3.axis=v.axis*t*0.1+r1.axis
    #r4.pos=v.axis*t*0.1+r1.axis
    #path2.append(pos=v.axis*t*0.1+r1.axis)
# wait ...
scene.mouse.getclick()
# reset these to leave previous path on display
r1.axis=(0,0,0)
r2.axis=(0,0,0)
r3.axis=(0,0,0)
a=vector(0.,-0.981,0)
scene.mouse.getclick()
# now animate vectors at constant a
for t in arange( t0,t1,0.01 ):
    rate(100)
    r2.pos=v.axis*t+r1.pos
    r2.axis=0.5*a*t*t
    r3.axis=0.5*a*t*t
    path2.append(pos=v.axis*t+r1.pos+r2.axis)
    monkey.pos=r3.pos+r3.axis
